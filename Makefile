#!make
include .env
export $(shell sed 's/=.*//' .env)

# Запуск
run: 
	cd app && ng serve

# Установка зависимостей
deps:
	cd app && npm install


genversion:
	rm -f $(VERSIONFILE)
	@echo "export const VERSION = \"$(VERSION)\";" > $(VERSIONFILE)


build_push: genversion
	@echo "Start build version $(VERSION)"
	docker build -t registry.gitlab.com/cast_iron_runner/frontend/frontend:$(VERSION) .
	@echo "Push version $(VERSION)"
	docker push registry.gitlab.com/cast_iron_runner/frontend/frontend:$(VERSION)
	@echo "Ready version $(VERSION)"
