import { Component, OnInit, OnDestroy } from '@angular/core';
import { Service } from './service'

enum states {
  ADD_URL = "ADD",
  DOWNLOADING = "DOWNLOADING",
  SELECTING = "SELECTING",
}


@Component({
  selector: 'add',
  templateUrl: './add.pug',
  styleUrls: ['./add.scss']
})
export class AddComponent implements OnInit, OnDestroy {
  title = 'add';

  state: states

  public st = states

  private service: Service

  response: any

  procent: number = 0;

  urls: string = `https://data.gov.ru/sites/default/files/3eba4626_perechen_avtozapravochnyh_stanciy.csv
https://data.gov.ru/sites/default/files/6770bd42_perechen_aptek_raspolozhennyh_na_territorii_municipaliteta.csv
https://data.gov.ru/sites/default/files/ccfe30db_perechen_bibliotek.csv
https://data.gov.ru/sites/default/files/7c919ced_perechen_doshkolnyh_obrazovatelnyh_organizaciy.csv
https://data.gov.ru/sites/default/files/2cec3d21_perechen_medicinskih_uchrezhdeniy_1.csv
https://data.gov.ru/sites/default/files/5cbea307_perechen_muzeev.csv`

  constructor(service: Service) {
    this.service = service
    this.state = states.ADD_URL
    // this.state = states.SELECTING
    this.updateTitle()
  }

  ngOnInit() {

  }

  start() {
    var ready = false
    var inter = setInterval(() => {
      if (this.procent >= 90) {
        clearInterval(inter)
      } else {
        this.procent = this.procent + 10
      }

      console.log('this.procent', this.procent)
    }, 1000)
    this.setState(states.DOWNLOADING)
    this.service
      .post('send', { data: this.urls })
      .subscribe((v: any) => {
        this.response = {}
        JSON.parse(v._body).forEach(element => {
          element.cols.forEach(e => {
            if (this.response[e] != undefined) {
              this.response[e]++
            } else {
              this.response[e] = 1
            }
          });
        });
        var element = []
        var keys = Object.keys(this.response)
        console.log('keys', keys);
        for (let index = 0; index < keys.length; index++) {
          var c = this.response[keys[index]];
          element.push({ count: this.response[keys[index]], name: keys[index] })
        }
        this.response = element


        this.procent = 100
        setTimeout(() => {
          this.endDownloading()
        }, 1000)
      })
  }


  // окончание процесса парсинга
  endDownloading() {
    this.setState(states.SELECTING)
  }

  // установка состояния
  setState(state: states) {
    this.state = state
    this.updateTitle()
  }

  // обновление заголовка
  updateTitle() {
    var title = ""
    switch (this.state) {
      case states.ADD_URL:
        title = "Добавить URL"
        break;

      case states.DOWNLOADING:
        title = "Идет выкачивание датасетов"
        break;

      case states.SELECTING:
        title = "Уточнение параметров"
        break;

      default:
        title = "Неизвестный науке заголовок"
        break;
    }

    this.title = title
  }


  ngOnDestroy() {

  }


}
