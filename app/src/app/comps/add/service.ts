import * as grpcWeb from 'grpc-web';
import { Http, Response } from '@angular/http';
import { Observable } from "rxjs";
import { environment } from '../../../environments/environment'
import { map, filter, catchError, mergeMap } from 'rxjs/operators';


export class Service {
    constructor(protected http: Http) {

    }


    /**
     * выполнение метода GET
     * @param method 
     * @param headers 
     */
    get(method: string, headers: any = {}, page?: number, filters?: string): Observable<any> {
        return this.http
            .get(`${environment.API_PATH}/${method}`, {})
    }

    /**
     * выполнение метода POST
     * @param method 
     * @param data 
     * @param headers 
    */
    post(method, data: any, headers: any = {}): Observable<any> {
        return this.http
            .post(`${environment.API_PATH}/${method}`, data, {})
    }

    /**
     * Разделение строке
     * @param str 
     * @param string 
     */
    splitLinks(str: string): string[] {
        return str.split("\n").map((v: string) => v.trim())
    }



}