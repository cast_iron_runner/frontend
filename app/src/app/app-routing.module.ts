import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddComponent } from './comps/add/add'
import {AppComponent} from './app.component'
import { from } from 'rxjs';
const routes: Routes = [
  {
    "path": "",
    "component": AddComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
