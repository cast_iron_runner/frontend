FROM node:10-alpine as builder
COPY ./app/package.json ./app/package-lock.json ./
RUN npm i && mkdir /ng-app && mv ./node_modules ./ng-app
WORKDIR /ng-app
COPY ./app .
# RUN $(npm bin)/ng add ng-cli-pug-loader
RUN $(npm bin)/ng build --output-path=dist --prod

FROM nginx:1.15.8-alpine

COPY nginx.conf /etc/nginx/conf.d/default.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /ng-app/dist /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]
